﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class boardManager : MonoBehaviour
{
    public GameObject codeRow;
    string [] codeColours= new string [4];
    int rng=1;
    public GameObject grid;
    public GameObject activeRow;
    public int activeRowNumber=0;
    public Button checkButton;
    public GameObject cursor;
    public Material selectedColour;
    public string selectedColourText;
    public Mesh selectedMesh;
    public AudioClip checkClip;
    public AudioSource checkSource;
     public AudioClip loseClip;
    public AudioSource loseSource;
     public AudioClip newColourClip;
    public AudioSource newColourSource;
     public AudioClip selectClip;
    public AudioSource selectSource;
     public AudioClip winClip;
    public AudioSource winSource;
    public bool cellMoving=false;
    public GameObject colourBoxes;


    public Material colourPink;
    public Material colourRed; // used for right position and right object.
    public Material colourBlack; // used for wrong position but right object.
    public Material camelMaterial; // used instead of colourRed
    public Mesh camelMesh;
    public GameObject camel;
    public GameObject camel2;
    public GameObject coin;
    public GameObject sphinx;
    public GameObject cobra;
    public GameObject pharoah;
    public GameObject comb;
    public Material coinMaterial; // was colourBlue
    public Mesh coinMesh;
    public Material sphinxMaterial; // was colourGreen
    public Mesh sphinxMesh;
    public Material cobraMaterial; // was colourYellow
    public Mesh cobraMesh;
    public Material pharoahMaterial; // was colourOrange
    public Mesh pharoahMesh;
    public Material combMaterial;  // was colourPurple
    public Mesh combMesh;
    

    // Start is called before the first frame update
    void Start()
    {
    checkSource.clip=checkClip;
    loseSource.clip=loseClip;
    newColourSource.clip=newColourClip;
    selectSource.clip=selectClip;
    winSource.clip=winClip;
        setCodeColours();
        activeRow=grid.transform.GetChild(activeRowNumber).gameObject; // sets active row initially to row 0.
        checkButton.transform.position=Vector3.zero;
        checkButton.transform.position = new Vector3(1200.0f, 100.0f, 0.0f); // //https://docs.unity3d.com/ScriptReference/Vector3.MoveTowards.html
    }
    /**
    * Assigns which objects will be used as part of the code.
     */
    void configureCode()
    {
        Vector3 [] coordinates={new Vector3(-3.0f,10.0f,0.0f),new Vector3(-1.0f,10.0f,0.0f),new Vector3(1.0f,10.0f,0.0f),new Vector3(3.0f,10.0f,0.0f),};
        GameObject codeObject;
        for(int i=0;i<4;i++)
        {
        codeRow.transform.GetChild(0).transform.parent=grid.transform;
        codeObject=grid.transform.GetChild(i+10).gameObject;
        codeObject.transform.position=Vector3.zero;
        codeObject.transform.position=coordinates[i];
        }
        for(int i=0;i<colourBoxes.transform.childCount;i++)
        {
            colourBoxes.transform.GetChild(i).gameObject.SetActive(false);
        }
    }
    int checkArray(string [] checkMe,string checkThisText)
    {
        int count=0;
        for(int i=0;i<checkMe.Length;i++)
        {
            if(checkMe[i]==checkThisText)
            {
                count++;
            }
        }
        return count;
    }
    void setCodeColours()
        {
           
        for(int i=0;i<codeColours.Length;i++)
        {
            
            rng=Random.Range(1,7);
            Debug.Log("rng = "+rng);
            
            if(rng==1)
            {
                
                if(checkArray(codeColours,"Camel")<1)
                {
                Debug.Log("1 activated");
                camel.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                codeColours[i]="Camel";
                }
                else
                {
                    rng=Random.Range(2,7); // made this 2 to 7 as to purposly exclude 1 as this saves computational resources.
                    i--;
                }
                
                
                /*
                if(checkArray(codeColours,"Camel")>0)
                {
                camel2.transform.parent=codeRow.transform;//https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                Debug.Log("do we get this???");
                }
                else
                {
                    camel.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html    
                    //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().material=colourRed;//https://answers.unity.com/questions/1315847/setting-materials-of-child-objects.html
                    //camel.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                    //codeRow.transform.GetChild(i).gameObject=camel;
                }
                codeColours[i]="Camel";
                */
                 
            }
            
            
            else if(rng==2)
            {
                
            if(checkArray(codeColours,"Coin")<1)
                {
                Debug.Log("2 activated");
                coin.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                codeColours[i]="Coin";
                }
                else
                {
                    rng=Random.Range(1,7);
                    i--;
                }
            //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().material=colourBlue;//https://answers.unity.com/questions/1315847/setting-materials-of-child-objects.html
            //coin.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
            //codeColours[i]="Coin";
            }
            else if(rng==3)
            {
                
            if(checkArray(codeColours,"Sphinx")<1)
                {
                Debug.Log("3 activated");
                sphinx.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                codeColours[i]="Sphinx";
                }
                else
                {
                    rng=Random.Range(1,7);
                    i--;
                }
            //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().material=colourGreen;//https://answers.unity.com/questions/1315847/setting-materials-of-child-objects.html
            //sphinx.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
            //codeColours[i]="Sphinx";
            }
            else if(rng==4)
            {
             if(checkArray(codeColours,"Cobra")<1)
                {
                Debug.Log("4 activated");
                cobra.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                codeColours[i]="Cobra";
                }
                else
                {
                    rng=Random.Range(1,7);
                    i--;
                }   
             //bM.cursor.transform.gameObject.GetComponent<MeshFilter>().mesh=bM.selectedMesh;
             //codeRow.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh=cobraMesh;    
            //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().material=colourYellow;//https://answers.unity.com/questions/1315847/setting-materials-of-child-objects.html
            //cobra.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
            //codeColours[i]="Cobra";
            }
            else if(rng==5)
            {
            if(checkArray(codeColours,"Pharoah")<1)
                {
                Debug.Log("5 activated");
                pharoah.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                codeColours[i]="Pharoah";
                }
                else
                {
                    rng=Random.Range(1,7);
                    i--;
                }    
            //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().material=colourOrange;//https://answers.unity.com/questions/1315847/setting-materials-of-child-objects.html
            //pharoah.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
            //codeColours[i]="Pharoah";
            }
            else if(rng==6)
            {
            if(checkArray(codeColours,"Comb")<1)
                {
                Debug.Log("6 activated");
                comb.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
                codeColours[i]="Comb";
                }
                else
                {
                    rng=Random.Range(1,6); // excludes number 6 to save computation resources.
                    i--;
                }
            //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().material=colourPurple;//https://answers.unity.com/questions/1315847/setting-materials-of-child-objects.html
            //comb.transform.parent=codeRow.transform; //https://answers.unity.com/questions/632792/how-to-make-an-object-a-child-of-another-object-sc.html
            //codeColours[i]="Comb";
            }
           //Debug.Log("rng = "+rng);
        }
    }
    /**
    Algorithm courtisty of https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array
    Translating from Java to C#
     */
    int [] rngArray()
    {
        int [] hintPositions = {0,1,2,3};

        for (int i=hintPositions.Length-1;i>0;i--)
        {
            rng=Random.Range(0,4);
            int indexToSwap=rng;
            int swapValue=hintPositions[indexToSwap];
            hintPositions[indexToSwap]=hintPositions[i];
            hintPositions[i]=swapValue;
        }
        return hintPositions;
    }
    int checkForColourInRow(int indexNumber)
    {
        int occ=0;
        int decrement=0;
        bool matchFound=false;
        for(int i=0;i<codeColours.Length;i++)
        {
            if(activeRow.transform.GetChild(indexNumber).gameObject.GetComponent<Renderer>().sharedMaterial==codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial)
            {
                occ++;
            }
            if(activeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial==codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial)
        {
            if(activeRow.transform.GetChild(indexNumber).gameObject.GetComponent<Renderer>().sharedMaterial==codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial)
            {
            Debug.Log("H1IIIIIIIII");
            matchFound=true;
            decrement++;
            }
        }
        }
        Debug.Log("do we make it at least here?");
        Debug.Log("indexNumber = "+indexNumber);
        if(activeRow.transform.GetChild(indexNumber).gameObject.GetComponent<Renderer>().sharedMaterial==codeRow.transform.GetChild(indexNumber).gameObject.GetComponent<Renderer>().sharedMaterial)
        {
            Debug.Log("what about hereeeee?");
            matchFound=true;
        }
        if(matchFound==false && occ>1)
        {
            occ=1;
            return occ;
        }
        if(matchFound==true)
        {
            if(occ>0)
            {
                Debug.Log("subtraction");
                return occ-decrement;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            if(occ>0)
            {
                return occ;
            }
    
        }
        return 0;
    }
    public int freeIndex(GameObject grid)
    {
        
        int i=0;
        while(i<codeColours.Length)
        {
            if(grid.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial!=colourBlack)
            {
                if(grid.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial!=colourRed)
                {
                    Debug.Log("The free index is : "+i);
                    return i; // returns the first free index.
                }
            }
        i++;
        }
        Debug.Log("NO FREE GRIDS AVAIABLE!");
        return -1;
    }
    public bool hint()
    {
        int occurance=0;
        GameObject hintGrid=activeRow.transform.GetChild(codeColours.Length).gameObject; // the fourth object in the row.
        for(int i=0;i<4;i++)
        {
            if(activeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial==colourPink)
            {
                return false;
            }
        }
        int [] hintNo=rngArray();
        for(int i=0;i<4;i++)
        {
            Debug.Log("hintNo["+i+"] "+"= "+hintNo[i]); // Debug line to test random sorting of hint array.
            hintGrid.transform.GetChild(hintNo[i]).gameObject.GetComponent<Renderer>().material=colourPink;//place a pink peg as default colour.
        }
        
        for(int i=0;i<codeColours.Length;i++)
        {
            Debug.Log("active row = "+activeRowNumber);
            Debug.Log("my selection = "+activeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial);
            Debug.Log("code selection = "+codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial);
            if(activeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial==codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial)
            {
                
                if(freeIndex(hintGrid)!=-1)
                {
                    hintGrid.transform.GetChild(freeIndex(hintGrid)).gameObject.GetComponent<Renderer>().material=colourRed;//place a red peg.
                }

            }
            else
            {
                occurance=checkForColourInRow(i);
                Debug.Log("Occurance = "+occurance);
                if(occurance>0)
                {
                    for(int index=0;index<occurance;index++)
                    {
                        if(freeIndex(hintGrid)!=-1)
                        {
                            hintGrid.transform.GetChild(freeIndex(hintGrid)).gameObject.GetComponent<Renderer>().material=colourBlack;
                        }
                    }
                }
            }
        }
        return true;
    }
    public void moveButton(float x,float y, float z)
    {
        //checkButton.transform.position=Vector3.zero;
       checkButton.transform.position = new Vector3(x*1200.0f, y*87.5f, z*0.0f);//https://docs.unity3d.com/ScriptReference/Vector3.MoveTowards.html 
    }
    
     public void check()
    {
        if(hint()==false)
        {
            return;
        }
        else
        {
        bool winCondition=true;
        int moveIndex=0;
        for(int i=0;i<codeColours.Length;i++)
        {
        //https://answers.unity.com/questions/786281/check-if-material-of-a-gameobject-a-material-varia.html
        if(winCondition== true && activeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial!=codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().sharedMaterial)
        {
            winCondition=false;
        }
      
        }
        if(winCondition==true)
        {
        Debug.Log("Player has won!");
        configureCode();
        winSource.Play();
        }
        else
        {
            if(activeRowNumber<9)
            {
        checkSource.Play();// only play once we know player has not either won or lost.
        Debug.Log("Player has not won on round : "+activeRowNumber);
        deactivateRow();// deactivates the row.
        //may want to disable previous rows here.
         activeRowNumber++;
         activeRow=grid.transform.GetChild(activeRowNumber).gameObject; // UPDATES ACTIVE ROW.
        activateRow();// activates the row
        
         //activeRow.transform.gameObject.GetComponent<Renderer>().enabled=true;
         //codeRow.transform.GetChild(i).gameObject.GetComponent<Renderer>().enabled=status;
        moveIndex=activeRowNumber+1;
        moveButton(1,moveIndex,1);

            }
            else
            {
                Debug.Log("Player has lost!");
                configureCode();
                loseSource.Play();
            }   
        }
        }
        
    }
    void deactivateRow()
    {
        for(int i=0;i<4;i++)
         {
        activeRow.transform.GetChild(i).gameObject.GetComponent<Collider>().enabled=false;
         }
         
    }
    
    void activateRow()
    {
        //activeRow.transform.gameObject.interactable(true);
        //activeRow.transform.GetChild(0).gameObject.interactable(false);
        for(int i=0;i<5;i++)
         {
         activeRow.transform.GetChild(i).gameObject.SetActive(true);    
         }
    }


    // Update is called once per frame
    void Update()
    { 
     cursor.transform.position=Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,20.0f));// https://www.youtube.com/watch?v=QNdFIkKdZIo  
      if (Input.GetKeyDown(KeyCode.Escape))
      {
        Application.Quit();
    }
}
}
