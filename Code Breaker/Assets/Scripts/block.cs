﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class block : MonoBehaviour
{
    public GameObject item;
    public boardManager bM;
    public Material mat;
    int blockID=0;
    public int [] blockPosition= new int [2];
    string blockColour="Pink"; // the Default colour for a block with no colour. No block should ever be displayed with the colour pink.
    
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    void blockConstructorDefault()
    {
        this.blockID=0;
        for(int i=0;i<=this.blockPosition.Length-1;i++)
        {
            this.blockPosition[i]=0;
        }
        blockColour="Pink";

    }
    void setBlockID(int setValue)
    {
        this.blockID=setValue;
    }
    int getBlockID()
    {
        return this.blockID;
    }
    void setBlockPosition(int [] setValue)
    {
        this.blockPosition = setValue;
    }
    int [] getBlockPosition()
    {
        return this.blockPosition;
    }
    void setBlockColour(string setValue)
    {
        this.blockColour=setValue;
        // will need to change material here.
    }
    string getBlockColour()
    {
        return this.blockColour;
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}
