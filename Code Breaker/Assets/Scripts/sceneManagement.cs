﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManagement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void reloadCurrentScene()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex); // https://answers.unity.com/questions/1109503/unity-53-restarting-current-scene.html
    }
    public void loadMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void loadPlay()
    {
        SceneManager.LoadScene(1);
    }
    public void loadAbout()
    {
        SceneManager.LoadScene(2);
    }
    public void loadSettings()
    {
        SceneManager.LoadScene(3);
    }
    public void loadStats()
    {
        SceneManager.LoadScene(4);
    }
    public void quit()
    {
        Application.Quit();//https://docs.unity3d.com/ScriptReference/Application.Quit.html
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
