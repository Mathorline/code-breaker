﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colourBlock : block
{
    public Material colour;
    public string colourText;
    
    
    // Start is called before the first frame update
    void Start()
    {
        Quaternion originalRotation=transform.rotation;
        Vector3 originalPosition=transform.position;
        transform.rotation=Quaternion.identity; // https://www.youtube.com/watch?v=wYAlky1aZn4
        transform.position=Vector3.zero;
        /**
        *https://docs.unity3d.com/ScriptReference/Mesh.CombineMeshes.html
         */
        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        int i = 0;
        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);

            i++;
        }
        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        transform.gameObject.SetActive(true);
        transform.rotation=originalRotation;
        transform.position=originalPosition;
    }
    void OnMouseDown()
    {
        bM.newColourSource.Play();
        bM.selectedColour=colour;
        bM.selectedMesh=transform.gameObject.GetComponent<MeshFilter>().mesh;   
        bM.cursor.transform.gameObject.GetComponent<Renderer>().material=colour;
        bM.cursor.transform.gameObject.GetComponent<MeshFilter>().mesh=bM.selectedMesh;
    }
    

    // Update is called once per frame
    void Update()
    {   
    }
}
