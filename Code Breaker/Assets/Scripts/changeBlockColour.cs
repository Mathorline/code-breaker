﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBlockColour : MonoBehaviour
{
    struct colourData
    {
        Material mat;
        string name;
        
        colourData constructor(Material matC,string nameC)
        {
            colourData data;
            data.mat=matC;
            data.name=nameC;
            return data;
        }

    }
    public Material colourPink;
    string Pink ="Pink";
    public Material colourRed;
    string Red="Red";
    public Material colourBlue;
    string Blue="Blue";
    public Material colourGreen;
    string Green="Green";
    public Material colourYellow;
    string Yellow="Yellow";
    public Material colourOrange;
    string Orange="Orange";
    public Material colourPurple;
    string Purple="Purple";
    public Material colourBlack;
    string Black="Black";
    int rng=1;

    public boardManager bM;
    public Material myColour;
     

    // Start is called before the first frame update
    void Start()
    {
        rng=Random.Range(1,7);
        
    }
    
    void setColour(string colour)
    {
        if(colour==Pink)
        {
        bM.selectedColour=colourPink;
        GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Black)
        {
        bM.selectedColour=colourBlack;
        GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Red)
        {
        bM.selectedColour=colourRed;
        GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Blue)
        {
            bM.selectedColour=colourBlue;
            GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Green)
        {
            bM.selectedColour=colourGreen;
            GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Yellow)
        {
            bM.selectedColour=colourYellow;
            GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Orange)
        {
            bM.selectedColour=colourOrange;
            GetComponent<Renderer>().material=bM.selectedColour;
        }
        if(colour==Purple)
        {
            bM.selectedColour=colourPurple;
            GetComponent<Renderer>().material=bM.selectedColour;
        }
    }
    
    void setRandomColour()
    {
        int oldRandom=rng;
        if(rng==1)
        {
        setColour(Red);
        }
        if(rng==2)
        {
        setColour(Blue);
        }
        if(rng==3)
        {
        setColour(Green);
        }
        if(rng==4)
        {
        setColour(Yellow);
        }
        if(rng==5)
        {
        setColour(Orange);
        }
        if(rng==6)
        {
        setColour(Purple);
        }
        while(oldRandom==rng)
        {
            rng=Random.Range(1,7);
        }

    }
    

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
    {
        
        GetComponent<Renderer>().material=bM.selectedColour;
    }   
    }
}
